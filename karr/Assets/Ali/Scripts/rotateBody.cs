﻿using UnityEngine;
using System.Collections;

public class rotateBody : MonoBehaviour {

	public GameObject wheel;
	private SkinnedMeshRenderer skinnedMeshRenderer;
	private Animator anim;
	private bool specialState = true;

	// Use this for initialization
	void Start () {
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.X)) {
			carSpecial();
		}
		transform.rotation = Quaternion.Euler (wheel.transform.eulerAngles.x, wheel.transform.eulerAngles.y, wheel.transform.eulerAngles.z);
	}
	void carSpecial(){
//		skinnedMeshRenderer.SetBlendShapeWeight (0, 0.5f);
//		skinnedMeshRenderer.SetBlendShapeWeight (1, 1f);
		if(specialState == false){
			anim.SetTrigger ("specialActiveOn");
			specialState = true;
		}
		else{
			anim.SetTrigger ("specialActiveOff");
		}
	}
}

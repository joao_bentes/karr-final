﻿using UnityEngine;
using System.Collections;

public class CarMachineGun : MonoBehaviour {
	
	//private AudioSource audioS;
	public GameObject aim;                  
	public float weaponRange = 50;
	public int weaponDamage = 20;
	//public AudioClip gunClip;
	
	// Use this for initialization
	void Start () {
		//audioS = new AudioSource();
		//audioS.clip = gunClip;
	}                    
	
	// Update is called once per frame
	
	void Update () {
		
		Debug.DrawRay(aim.gameObject.transform.position, aim.gameObject.transform.forward * weaponRange, Color.red);
		
		if (Input.GetMouseButtonDown (0)) {
			
			RaycastHit hit;
			Ray gunRay = new Ray (aim.gameObject.transform.position, aim.gameObject.transform.forward);


			
			//audioS.Play();
			
			//Debug.Log (audioS.clip);
			Debug.Log ("pew");
			
			if (Physics.Raycast (gunRay, out hit, weaponRange)) {
				
				if (hit.collider.gameObject.tag == "destructable" || hit.collider.gameObject.tag == "Player 1" || hit.collider.gameObject.tag == "Player 2" || hit.collider.gameObject.tag == "Player 3" || hit.collider.gameObject.tag == "Player 4") {
					Debug.Log ("SHOT");
				}
			}
		}
	}
}
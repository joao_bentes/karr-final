using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use
		private int playerNumber;


        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
			playerNumber = 0;

        }


        private void FixedUpdate()
        {
			if (this.tag == "Player 1") {
				playerNumber = 1;
			}
			else if (this.tag == "Player 2") {
				playerNumber = 2;
			}
			else if (this.tag == "Player 3") {
				playerNumber = 3;
			}
			else if (this.tag == "Player 4") {
				playerNumber = 4;
			}
            // pass the input to the car!
			if (playerNumber == 1) {
				Debug.Log (playerNumber);
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P1 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P1 Vertical");
				//float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, 0.0f);

			}
			if (playerNumber == 2) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P2 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P2 Vertical");

				//float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, 0.0f);

			}
			if (playerNumber == 3) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P3 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P3 Vertical");
				//float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, 0.0f);

			}
			if (playerNumber == 4) {
				float h;
				float v;
				h = CrossPlatformInputManager.GetAxis("P4 Horizontal");
				v = CrossPlatformInputManager.GetAxis("P4 Vertical");
				//float handbrake = CrossPlatformInputManager.GetAxis("Jump");
				m_Car.Move(h, v, v, 0.0f);

			}
//            float h = CrossPlatformInputManager.GetAxis("Horizontal");
//            float v = CrossPlatformInputManager.GetAxis("Vertical");
#if !MOBILE_INPUT
//            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
//            m_Car.Move(h, v, v, handbrake);
#else
            m_Car.Move(h, v, v, 0f);
#endif
        }

//		if(playerNum==1)
//		{
//			value=Input.GetAxis("Player1 left/right");
//		}
//		else if(playerNum==2)
//		{
//			value=Input.GetAxis("Player2 left/right");
//		}
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class cloneSpawn : MonoBehaviour {
	
	private bool cloneSpecialAvailable = true;
//	private bool cloneActivated = false;
	
	public GameObject sniperClone;
	
	public List<GameObject> spawnPoints;
	
	private int currentCloneTimer;
	
	void translatePlayer (GameObject sniperPlayer) {
		GameObject randomSpawnPoint = spawnPoints[Random.Range (0, spawnPoints.Count)];
		if (randomSpawnPoint.GetComponent<respawnCar>().isFree == true) {
			//translate current real sniper player to a spawnpoint
//			Vector3 randomSpawnPointPosition = randomSpawnPoint.transform.position;
//			Vector3 randomSpawnPointRotation = Quaternion.Euler (randomSpawnPoint.transform.rotation);
//			Rigidbody.position = randomSpawnPointPosition;
//			Rigidbody.rotation = randomSpawnPointRotation;
			Instantiate (this.gameObject, randomSpawnPoint.transform.position, randomSpawnPoint.transform.rotation);
		} else {
			translatePlayer(this.gameObject);
		}
	}
	
	void setNewCloneTimer () {
		currentCloneTimer = 10;
		InvokeRepeating ("countdownCloneTimer", 1f, 1f);
	}
	
	void countdownCloneTimer () {
		currentCloneTimer--;
		print (currentCloneTimer);
		if (currentCloneTimer == 0) {
			CancelInvoke ("countdownCloneTimer");
			//            print ("Invoke Cancelled");
			cloneSpecialAvailable = true;
			//            print ("Special reset");
		}
	}
	
	void clonePlayer () {
		//get current speed / direction
		Instantiate (sniperClone, this.transform.position, this.transform.rotation);
		//set clone to player speed / direction
		translatePlayer (this.gameObject);
		cloneSpecialAvailable = false;
		setNewCloneTimer();
	}
	
	void Update () {
		if (Input.GetButton("Fire1")) {
			if (cloneSpecialAvailable == true) {
				clonePlayer ();
			} 
		}
	}
}

//function Update () { if (Input.GetKeyDown("e") /&& player is in tank/) 
	// commented out for your code to replace { tank = GameObject.Find("Tank"); 
	// finds the GameObject named if ( tank ) { player.position = (tank.position + (2,0,0)); player.rotation = tank.rotation; 
	// make the player visible here } } }
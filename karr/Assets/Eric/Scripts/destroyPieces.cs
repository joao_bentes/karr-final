﻿using UnityEngine;
using System.Collections;

public class destroyPieces : MonoBehaviour {

	public GameObject explodeParticle;
	private int radNum = Random.Range (3, 7);

	// Use this for initialization
	void Start () {
		Destroy (gameObject, radNum);
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "sniper") {
			Vector3 offset = new Vector3 (0, .5f, 0);
			Instantiate (explodeParticle, transform.position + offset, transform.rotation);
			Destroy (gameObject);
		}
	}
}

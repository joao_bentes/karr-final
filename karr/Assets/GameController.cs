﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	//Game State
	private bool isOnMatch;

	void Awake() {
		DontDestroyOnLoad(this);
	}
	// Use this for initialization
	void Start () {
		isOnMatch = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetIsOnMatch(bool b)
	{
		isOnMatch = b;
	}

	public bool GetIsOnMatch() {
		return isOnMatch;
	}
}

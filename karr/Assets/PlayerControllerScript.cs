﻿using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour {

	//Car State
	private bool isInstantiated;

	//Reference for Game Controller
	private GameObject gC;
	private GameController gameController;

	//Reference for Camera
	private GameObject cam;

	//Current Car
	private GameObject currentCar;
	
	//CAR STATS
	private GameObject currentCarStats;
	private CarStats stats;
	
	private float armorBulletModifier;
	private float armorMeleeModifier;
	
	private float damageBulletModifier;
	private float damageMeleeModifier;
	
	private float hpTotal;
	private float specialCooldownCap;
	
	private float rateOfFire;
	private float rangeOfBullets;
	
	//PLAYER STATS
	private float currentHp;
	public float currentSpeed;
	
	public int currentPlayer;
	public string currentCarType;

	void Awake() {
		DontDestroyOnLoad(this);
	}
	// Use this for initialization
	void Start () {

		isInstantiated = false;

		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();

		//for testing
		armorBulletModifier = 20;
		armorMeleeModifier = 50;
		
		damageBulletModifier = 50;
		damageMeleeModifier = 50;
		
		hpTotal = 100;
		specialCooldownCap = 100;
		
		rateOfFire = 100;
		rangeOfBullets = 100;
		
		currentHp = hpTotal;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (gameController.GetIsOnMatch () && Application.loadedLevel == 1) {

				if (isInstantiated) {
					
					if(this.currentCar != null)
					{	
						SetCurrentSpeed ();

						if (this.currentHp <= 0) {
							this.CarDeath ();
						}
					
						if (Input.GetMouseButtonDown (0)) {
							CarPrimaryWeaponShoot ();
						}
					}
				} 
				
				else {
					GameObject respawn = GameObject.FindGameObjectWithTag ("Respawn " + currentPlayer);
					GameObject c = Instantiate (stats.carPrefab, respawn.transform.position, respawn.transform.rotation) as GameObject;
					c.tag = "Player "+ currentPlayer;
					SetCurrentCar ();

					cam = GameObject.FindGameObjectWithTag ("Camera " + currentPlayer);
					isInstantiated = true;
				}
		
		}

		
	}
	
	public void SetCurrentCarStats(GameObject ccs) {
		currentCarStats = ccs;
		stats = currentCarStats.GetComponent<CarStats> ();
		SetPlayerCarStats ();
	}
	
	public void SetPlayerCarStats () {
		armorBulletModifier = stats.GetCarArmorBulletModifier ();
		armorMeleeModifier = stats.GetCarArmorMeleeModifier ();
		
		damageBulletModifier = stats.GetCarDamageBulletModifier ();
		damageMeleeModifier = stats.GetCarDamageMeleeModifier ();
		
		hpTotal = stats.GetCarHpTotal ();
		specialCooldownCap = stats.GetCarSpecialCooldownCap ();
		
		rateOfFire = stats.GetCarRateOfFire ();
		rangeOfBullets = stats.GetCarRangeOfBullets ();
	}
	
	public void SetCurrentCar() {
		if (this.currentPlayer == 1) {
			currentCar = GameObject.FindWithTag ("Player 1");
		}
		else if (this.currentPlayer == 2) {
			currentCar = GameObject.FindWithTag ("Player 2");
		}
		else if (this.currentPlayer == 3) {
			currentCar = GameObject.FindWithTag ("Player 3");
		}
		else if (this.currentPlayer == 4) {
			currentCar = GameObject.FindWithTag ("Player 4");
		}
	}
	
	public void SetCurrentSpeed() {
		Rigidbody carRigidBody;
		
		carRigidBody = currentCar.gameObject.GetComponent<Rigidbody> ();
		currentSpeed = carRigidBody.velocity.magnitude;
	}
	
	
	public void CarPrimaryWeaponShoot() {
		
		RaycastHit hit;
		Ray gunRay;
		
		Transform aim = currentCar.transform.Find ("Aim");
		
		gunRay	= new Ray (aim.gameObject.transform.position, aim.forward);
		
		Debug.DrawRay(aim.position, aim.forward * rangeOfBullets, Color.red);
		
		if (Physics.Raycast (gunRay, out hit, rangeOfBullets)) {
			Debug.Log ("shot");
			Debug.Log (hit.collider.gameObject.name);

			if (hit.collider.gameObject.tag == "destructable") 
			{

			}
			else if (hit.collider.gameObject.tag == "Player 1") 
			{
				Debug.Log ("P 1");
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 1");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				Debug.Log(hitController.currentPlayer);
				hitController.CarGotHitBullet(this.damageBulletModifier);
	
				
			}
			else if (hit.collider.gameObject.tag == "Player 2") 
			{
				Debug.Log ("P 2");
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 2");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
			else if (hit.collider.gameObject.tag == "Player 3") 
			{
				Debug.Log ("P 3");
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 3");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
			else if (hit.collider.gameObject.tag == "Player 4") 
			{
				Debug.Log ("P 4");
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 4");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
			else if (hit.collider.gameObject.tag == "TEST") 
			{
				Debug.Log ("TEST");
				GameObject hitObject = GameObject.FindGameObjectWithTag("Controller 2");
				PlayerControllerScript hitController = hitObject.GetComponent<PlayerControllerScript>();
				hitController.CarGotHitBullet(this.damageBulletModifier);
				
			}
		}
		
	}
	
	public void CarGotHitBullet ( float attackerDamage )
	{
		Debug.Log("I got shot by someone");
		this.currentHp -= attackerDamage - this.armorBulletModifier;
		Debug.Log (this.currentHp);
	}
	
	public void CarDeath ()
	{
		Destroy(currentCar);
	}

	
	// TEST METHIODS
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Cube") {
			SetCurrentCarStats (other.gameObject);
			PrintStats();
		}
	}
	
	public void PrintStats () {
		Debug.Log (armorBulletModifier);
		Debug.Log (armorMeleeModifier);
		
		Debug.Log (damageBulletModifier);
		Debug.Log (damageMeleeModifier);
		
		Debug.Log (hpTotal);
		Debug.Log (specialCooldownCap);
		
		Debug.Log (rateOfFire);
		Debug.Log (rangeOfBullets);
	}
}

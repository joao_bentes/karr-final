﻿using UnityEngine;
using System.Collections;

public class SelectIconController : MonoBehaviour {

	//Reference for Game Controller
	private GameObject gC;
	private GameController gameController;

	public float pos;
	public float originalX;
	public int cursorPlayer;
	private GameObject car;
	private GameObject playerController;
	public PlayerControllerScript controllerScript;
	// Use this for initialization
	void Start () {

		gC = GameObject.FindGameObjectWithTag ("Game Controller");
		gameController = gC.GetComponent<GameController> ();

		pos = 0;
		originalX = this.gameObject.transform.position.x;
		Debug.Log ("Controller " + this.cursorPlayer);
		playerController = GameObject.FindGameObjectWithTag("Controller " + this.cursorPlayer);
		controllerScript = playerController.GetComponent<PlayerControllerScript>();
	}
	
	// Update is called once per frame
	void Update () {

		if (this.tag == "Cursor 1") {
			if (Input.GetKeyDown (KeyCode.D)) {
				pos++;
				pos = pos % 4;
				if (pos > 0) {
					this.transform.Translate (4, 0, 0);
				} else {
					this.transform.Translate (-12, 0, 0);
				}
			}

			if (Input.GetKeyDown (KeyCode.A)) {
				pos--;
				if (pos < 0) {
					pos = 3;
				}

				if (pos < 3) {
					this.transform.Translate (-4, 0, 0);
				} else {
					this.transform.Translate (12, 0, 0);
				}
			}

			if (Input.GetKeyDown (KeyCode.X)) {
				car = GameObject.FindGameObjectWithTag("Car " + pos);
				controllerScript.SetCurrentCarStats(car);
				controllerScript.PrintStats();

			}
		}
		else if (this.tag == "Cursor 2") {
			if (Input.GetKeyDown (KeyCode.T)) {
				pos++;
				pos = pos % 4;
				if (pos > 0) {
					this.transform.Translate (4, 0, 0);
				} else {
					this.transform.Translate (-12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.E)) {
				pos--;
				if (pos < 0) {
					pos = 3;
				}
				
				if (pos < 3) {
					this.transform.Translate (-4, 0, 0);
				} else {
					this.transform.Translate (12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.C)) {
				car = GameObject.FindGameObjectWithTag("Car " + pos);
				controllerScript.SetCurrentCarStats(car);
				controllerScript.PrintStats();

			}
		}

		else if (this.tag == "Cursor 3") {
			if (Input.GetKeyDown (KeyCode.J)) {
				pos++;
				pos = pos % 4;
				if (pos > 0) {
					this.transform.Translate (4, 0, 0);
				} else {
					this.transform.Translate (-12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.G)) {
				pos--;
				if (pos < 0) {
					pos = 3;
				}
				
				if (pos < 3) {
					this.transform.Translate (-4, 0, 0);
				} else {
					this.transform.Translate (12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.V)) {
				car = GameObject.FindGameObjectWithTag("Car " + pos);
				controllerScript.SetCurrentCarStats(car);
				controllerScript.PrintStats();

			}
		}

		else if (this.tag == "Cursor 4") {
			if (Input.GetKeyDown (KeyCode.O)) {
				pos++;
				pos = pos % 4;
				if (pos > 0) {
					this.transform.Translate (4, 0, 0);
				} else {
					this.transform.Translate (-12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.U)) {
				pos--;
				if (pos < 0) {
					pos = 3;
				}
				
				if (pos < 3) {
					this.transform.Translate (-4, 0, 0);
				} else {
					this.transform.Translate (12, 0, 0);
				}
			}
			
			if (Input.GetKeyDown (KeyCode.B)) {
				car = GameObject.FindGameObjectWithTag("Car " + pos);
				controllerScript.SetCurrentCarStats(car);
				controllerScript.PrintStats();
			}
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			gameController.SetIsOnMatch (true);
			Application.LoadLevel (1);
		}
	}
}
